
//Conditional soal no 1

var nama =  ""
var peran = ""

// Output untuk Input nama = '' dan peran = ''
if (nama == "") {
  console.log("nama harus diisi!!!");
}
 
//Output untuk Input nama = 'John' dan peran = ''
"Halo John, Pilih peranmu untuk memulai game!"
if (nama == "John") {
  console.log("Halo "+ nama +", Pilih peranmu untuk memulai game!");
}

//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
"Selamat datang di Dunia Werewolf, Jane"
"Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
if (nama == "Jane" ) {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
}

if (peran == "Penyihir" ) {
  console.log("Halo "+ peran + " " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
}

//Output untuk Input nama = 'Jenita' dan peran 'Guard'
"Selamat datang di Dunia Werewolf, Jenita"
"Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."

if (nama == "Jenita" ) {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
}

if (peran == "Guard" ) {
  console.log("Halo "+ peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
}
 
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
"Selamat datang di Dunia Werewolf, Junaedi"
"Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!"

if (nama == "Junaedi" ) {
  console.log("Selamat datang di Dunia Werewolf, " + nama);
}

if (peran == "Werewolf" ) {
  console.log("Halo "+ peran + " " + nama + ", Kamu akan memakan mangsa setiap malam!");
}


// soal switch case
var tanggal = 21; 
var bulan = 1; 
var tahun = 1945;

switch (tanggal) {
  case 1: { console.log('1'); break; }
  case 2: { console.log('2'); break; }
  case 3: { console.log('3'); break; }
  case 4: { console.log('1'); break; }
  default: { console.log('21'); }
}

switch (bulan) {
  case 1: { console.log('Januari'); break; }
  case 2: { console.log('February'); break; }
  case 3: { console.log('Maret'); break; }
  case 4: { console.log('April'); break; }
  case 5: { console.log('Mei'); break; }
  case 6: { console.log('Juni'); break; }
  case 7: { console.log('Juli'); break; }
  case 8: { console.log('Agustus'); break; }
  case 9: { console.log('September'); break; }
  case 10: { console.log('Oktober'); break; }
  case 11: { console.log('November'); break; }
  case 12: { console.log('Desember'); break; }
  default: { console.log('Bulan tidak terdaftar'); }
}

switch (tahun) {
  case 1900: { console.log('1900'); break; }
  case 1901: { console.log('1901'); break; }
  case 1902: { console.log('1902'); break; }
  case 1945: { console.log('1945'); break; }
  default: { console.log('2200'); }
}
