// Soal no 1
// LOOPING PERTAMA//
var flag = 1;
var judul = "LOOPING PERTAMA";
 console.log (judul);
while(flag < 20) { // Loop akan terus berjalan selama nilai flag masih dibawah 20
  console.log( (flag+=1) +" - I love coding"); // Menampilkan nilai flag pada kelipatan dua
  flag++; // Mengubah nilai flag dengan menambahkan 1
}

// //LOOPING KEDUA//
var nomer = 21;
var angka = "LOOPING KEDUA";
 console.log (angka);
while(nomer > 1) { // Loop akan terus berjalan selama nilai flag masih dibawah 20
  console.log( (nomer-=1) +" - I will become a mobile developer"); // Menampilkan nilai flag pada kelipatan dua
  nomer--; // Mengubah nilai flag dengan menambahkan 1
}


//SOAL NO 2
// LOOPING FOR//
for(var kata = 1; kata < 21;kata++) {
    if((kata%3) === 0) {
        console.log(kata+' - I Love Coding')
    }
    else if((kata%2) === 0) {
        console.log(kata+' - Berkualitas')
}
    else {
    console.log(kata+' - santai')
}
}

//SOAL NO 3
//MEMBUAT PERSEGI PANJANG//
for(var pagar = 1; pagar < 5;pagar++) {
    console.log('########');
}

//SOAL NO 4
//Membuat Tangga//

for(var tangga = 1; tangga < 7;tangga++) {
            if((tangga) === 1) {
                console.log('#')
            }
            else if((tangga) === 2) {
                console.log('##')
            }
            else if((tangga) === 3) {
                console.log('###') 
            }
            else if((tangga) === 4) {
                console.log('####')
            }
            else if((tangga) === 5) {
                console.log('#####')
            }
            else if((tangga) === 6) {
                console.log('######')
            } else {
                console.log('#######')
        }
    }


//SOAL NO 5
// Membuat Papan Catur
for(var kata = 1; kata < 13;kata++) {
        if((kata%3) === 0) {
            console.log(' # # # # # # # #')
        }
        else if((kata%2) === 0) {
            console.log('# # # # # # # #')
        }
    }